package org.practice.dto;

import jakarta.validation.constraints.Min;
import org.bson.types.ObjectId;

public class ProductShopRecord {
    private ObjectId categoryId;
    private ObjectId shopId;
    private ObjectId productId;
    @Min(value = 1000, message = "Amount of products can't be less than 1000!")
    private int amount;

    public ObjectId getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(ObjectId categoryId) {
        this.categoryId = categoryId;
    }

    public ObjectId getProductId() {
        return productId;
    }

    public void setProductId(ObjectId productId) {
        this.productId = productId;
    }

    public ObjectId getShopId() {
        return shopId;
    }

    public void setShopId(ObjectId shopId) {
        this.shopId = shopId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
