package org.practice;

import com.github.javafaker.Faker;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Indexes;
import org.apache.commons.lang3.time.StopWatch;
import org.bson.Document;
import org.practice.db.DBInitializer;
import org.practice.db.DBWriter;
import org.practice.properties.PropertiesLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import static com.mongodb.client.model.Accumulators.sum;
import static com.mongodb.client.model.Aggregates.*;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Projections.fields;
import static com.mongodb.client.model.Projections.include;
import static com.mongodb.client.model.Sorts.descending;
import static org.practice.db.DBWriter.STORAGE_COLLECTION_NAME;
import static org.practice.db.ProductInitializer.CATEGORY_COLLECTION_NAME;
import static org.practice.db.ProductInitializer.PRODUCTS_COLLECTION_NAME;
import static org.practice.db.ShopInitializer.SHOP_COLLECTION_NAME;

public class App {
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);
    public static final String DB_NAME = "practice_nosql";
    private static final String CATEGORY_BY_DEFAULT = "Design";

    public static void main(String[] args) {
        StopWatch stopWatch = new StopWatch();
        Properties properties = new PropertiesLoader().loadProperties();
        Faker faker = new Faker(new Locale("uk"));

        int numOfCategories = Integer.parseInt(properties.getProperty("numOfCategories"));
        int numOfProducts = Integer.parseInt(properties.getProperty("numOfProducts"));
        int numOfShops = Integer.parseInt(properties.getProperty("numOfShops"));
        int numOfProductShopRecords = Integer.parseInt(properties.getProperty("numOfProductShopRecords"));
        int numOfThreads = Integer.parseInt(properties.getProperty("numOfThreads"));

        try (MongoClient connection = MongoClients.create(properties.getProperty("url"))) {
            DBInitializer initializer = new DBInitializer(connection, faker, numOfCategories, numOfProducts, numOfShops);
            DBWriter writer = new DBWriter(connection, numOfProductShopRecords, numOfProducts, numOfShops, numOfThreads);
            setupDb(connection);
            stopWatch.start();

            initializer.run();
            writer.run();

            stopWatch.stop();
            double time = stopWatch.getTime() / 1000.0;
            LOGGER.info("TIME: {} s. WPS: {}", time, numOfProductShopRecords / time);

            stopWatch.reset();
            stopWatch.start();

            String address = formatAddressDocument(executeQuery(connection, getCategoryName(args)));

            stopWatch.stop();
            LOGGER.info("QUERY TIME: {} s. Shop address with most products: {}", stopWatch.getTime() / 1000.0, address);
        }
    }

    public static String formatAddressDocument(Document address) {
        StringBuilder sb = new StringBuilder();
        sb.append(address.get("city"))
                .append(", ")
                .append(address.get("street"))
                .append(" ")
                .append(address.get("house"));
        return sb.toString();
    }

    public static String getCategoryName(String[] args) {
        if (args.length != 0) {
            LOGGER.info("Using category from command line: {}", args[0]);
            return args[0];
        } else {
            LOGGER.info("Using category by default: {}", CATEGORY_BY_DEFAULT);
            return CATEGORY_BY_DEFAULT;
        }
    }

    public static Document executeQuery(MongoClient connection, String categoryName) {
        MongoDatabase db = connection.getDatabase(DB_NAME);

        MongoCollection<Document> categories = db.getCollection(CATEGORY_COLLECTION_NAME);
        List<Document> categoryId = categories.aggregate(List.of(
                match(eq("categoryInfo.categoryName", categoryName)),
                project(fields(include("_id")))
        )).into(new ArrayList<>());

        MongoCollection<Document> storage = db.getCollection(STORAGE_COLLECTION_NAME);
        List<Document> shopId = storage.aggregate(List.of(
                match(eq("storageInfo.categoryID", categoryId.get(0).get("_id"))),
                group("$storageInfo.shopID", sum("totalAmount", "$storageInfo.amount")),
                sort(descending("totalAmount")),
                limit(1)
        )).into(new ArrayList<>());

        MongoCollection<Document> shops = db.getCollection(SHOP_COLLECTION_NAME);

        Document shop = shops.find(eq("_id", shopId.get(0).get("_id"))).into(new ArrayList<>()).get(0);
        return ((Document)shop.get("address"));
    }

    public static void setupDb(MongoClient connection) {
        MongoDatabase db = connection.getDatabase(DB_NAME);
        MongoCollection<Document> categories = db.getCollection(CATEGORY_COLLECTION_NAME);
        MongoCollection<Document> products = db.getCollection(PRODUCTS_COLLECTION_NAME);
        MongoCollection<Document> shops = db.getCollection(SHOP_COLLECTION_NAME);
        MongoCollection<Document> storage = db.getCollection(STORAGE_COLLECTION_NAME);
        categories.drop();
        products.drop();
        shops.drop();
        storage.drop();
//        storage.createIndex(Indexes.ascending("storageInfo.shopID"));
        storage.createIndex(Indexes.ascending("storageInfo.categoryID"));
    }
}
