package org.practice.db;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import org.bson.Document;
import org.practice.generators.ProductShopGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import static org.practice.App.DB_NAME;
import static org.practice.db.DBWriter.BATCH_SIZE;
import static org.practice.db.DBWriter.STORAGE_COLLECTION_NAME;

public class DBThread implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(DBThread.class);
    private static final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
    private final ProductShopGenerator generator;
    private final MongoClient connection;
    private final int numOfRecords;

    public DBThread(ProductShopGenerator generator, MongoClient connection, int numOfRecords) {
        this.generator = generator;
        this.connection = connection;
        this.numOfRecords = numOfRecords;
    }

    @Override
    public void run() {
        MongoCollection<Document> collection = connection.getDatabase(DB_NAME).getCollection(STORAGE_COLLECTION_NAME);
        LOGGER.info("DB Thread started");
        List<Document> list = new ArrayList<>();
        AtomicInteger counter = new AtomicInteger();
        Stream.generate(generator::generateProductShopRecord)
                .filter(prod -> validator.validate(prod).isEmpty())
                .limit(numOfRecords)
                .forEach(prod -> {
                    Document storageInfo = new Document("categoryID", prod.getCategoryId());
                    storageInfo.append("productID", prod.getProductId())
                            .append("shopID", prod.getShopId())
                            .append("amount", prod.getAmount());
                    list.add(new Document("storageInfo", storageInfo));
                    if (list.size() >= BATCH_SIZE) {
                        collection.insertMany(list);
                        counter.addAndGet(list.size());
                        LOGGER.info("Generated {} records out of {}", counter.get(), numOfRecords);
                        list.clear();
                    }
                });
        collection.insertMany(list);
        LOGGER.info("DB Thread stopped. Records generated: {}", numOfRecords);
    }
}
