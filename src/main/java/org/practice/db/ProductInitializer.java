package org.practice.db;

import com.github.javafaker.Faker;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import org.bson.Document;
import org.practice.generators.ProductGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import static com.mongodb.client.model.Projections.fields;
import static com.mongodb.client.model.Projections.include;
import static org.practice.App.DB_NAME;
import static org.practice.db.DBWriter.BATCH_SIZE;

public class ProductInitializer implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProductInitializer.class);
    private static final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
    public static final String CATEGORY_COLLECTION_NAME = "categories";
    public static final String PRODUCTS_COLLECTION_NAME = "products";
    private final MongoClient connection;
    private final Faker faker;
    private final int numOfCategories;
    private final int numOfProducts;

    public ProductInitializer(MongoClient connection, Faker faker, int numOfCategories, int numOfProducts) {
        this.connection = connection;
        this.faker = faker;
        this.numOfCategories = numOfCategories;
        this.numOfProducts = numOfProducts;
    }

    @Override
    public void run() {
        LOGGER.info("Product Initializer started");
        MongoDatabase database = connection.getDatabase(DB_NAME);
        MongoCollection<Document> categoryCollection = database.getCollection(CATEGORY_COLLECTION_NAME);
        MongoCollection<Document> productCollection = database.getCollection(PRODUCTS_COLLECTION_NAME);
        generateCategories(categoryCollection, numOfCategories);
        List<Document> categoriesId = categoryCollection.find()
                .projection(fields(include("_id")))
                .into(new ArrayList<>());
        generateProducts(productCollection, new ProductGenerator(faker, categoriesId, numOfCategories), numOfProducts);
        LOGGER.info("Product Initializer stopped");
    }

    private void generateProducts(MongoCollection<Document> collection, ProductGenerator generator, int numOfProducts) {
        LOGGER.info("Start of product generation");
        List<Document> list = new ArrayList<>();
        AtomicInteger counter = new AtomicInteger();
        Stream.generate(generator::generateProduct)
                .filter(product -> validator.validate(product).isEmpty())
                .limit(numOfProducts)
                .forEach(product -> {
                    Document productInfo = new Document("productName", product.getName());
                    productInfo.append("categoryID", product.getCategoryId());
                    Document document = new Document("productInfo", productInfo);
                    list.add(document);
                    if (list.size() >= BATCH_SIZE) {
                        collection.insertMany(list);
                        counter.addAndGet(list.size());
                        LOGGER.info("Generated {} products out of {}", counter.get(), numOfProducts);
                        list.clear();
                    }
                });
        collection.insertMany(list);
        LOGGER.info("End of product generation. Products generated: {}", numOfProducts);
    }

    private void generateCategories(MongoCollection<Document> collection, int numOfCategories) {
        LOGGER.info("Start of category generation");
        List<Document> list = new ArrayList<>();
        AtomicInteger counter = new AtomicInteger();
        Stream.generate(() -> faker.company().industry())
                .limit(numOfCategories - 1)
                .forEach(category -> {
                    Document document = new Document("categoryInfo", new Document("categoryName", category));
                    list.add(document);
                    if (list.size() >= BATCH_SIZE) {
                        collection.insertMany(list);
                        counter.addAndGet(list.size());
                        LOGGER.info("Generated {} categories out of {}", counter.get(), numOfProducts);
                        list.clear();
                    }
                });
        Document document = new Document("categoryInfo", new Document("categoryName", "Design"));
        list.add(document);
        collection.insertMany(list);
        LOGGER.info("End of category generation. Categories generated: {}", numOfCategories);
    }
}