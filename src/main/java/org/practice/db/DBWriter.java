package org.practice.db;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.practice.generators.ProductShopGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static com.mongodb.client.model.Projections.fields;
import static com.mongodb.client.model.Projections.include;
import static org.practice.App.DB_NAME;
import static org.practice.db.ProductInitializer.CATEGORY_COLLECTION_NAME;
import static org.practice.db.ProductInitializer.PRODUCTS_COLLECTION_NAME;
import static org.practice.db.ShopInitializer.SHOP_COLLECTION_NAME;

public class DBWriter implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(DBWriter.class);
    public static final String STORAGE_COLLECTION_NAME = "storage";
    public static final int BATCH_SIZE = 10_000;
    private final MongoClient connection;
    private final int numOfStorageRecords;
    private final int numOfProducts;
    private final int numOfShops;
    private final int numOfThreads;

    public DBWriter(MongoClient connection, int numOfProductShopRecords, int numOfProducts, int numOfShops, int numOfThreads) {
        this.numOfStorageRecords = numOfProductShopRecords;
        this.connection = connection;
        this.numOfProducts = numOfProducts;
        this.numOfShops = numOfShops;
        this.numOfThreads = numOfThreads;
    }

    @Override
    public void run() {
        LOGGER.info("DB Writer started");
        MongoDatabase db = connection.getDatabase(DB_NAME);
        MongoCollection<Document> categoryCollection = db.getCollection(CATEGORY_COLLECTION_NAME);
        MongoCollection<Document> productCollection = db.getCollection(PRODUCTS_COLLECTION_NAME);
        MongoCollection<Document> shopCollection = db.getCollection(SHOP_COLLECTION_NAME);
        List<Document> categoryIds = categoryCollection.find()
                .projection(fields(include("_id")))
                .into(new ArrayList<>());
        List<Document> productIds = productCollection.find()
                .projection(fields(include("_id")))
                .into(new ArrayList<>());
        List<Document> shopIds = shopCollection.find()
                .projection(fields(include("_id")))
                .into(new ArrayList<>());
        ProductShopGenerator generator = new ProductShopGenerator(categoryIds, productIds, shopIds);
        ExecutorService executorService = Executors.newFixedThreadPool(numOfThreads);

        int[] recordsPerThread = new int[numOfThreads];
        Arrays.fill(recordsPerThread, numOfStorageRecords / numOfThreads);
        if (numOfStorageRecords % numOfThreads != 0) {
            recordsPerThread[0] += numOfStorageRecords % numOfThreads;
        }

        for (int i = 0; i < numOfThreads; i++) {
            executorService.execute(new DBThread(generator, connection, recordsPerThread[i]));
        }
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(60, TimeUnit.MINUTES)) {
                LOGGER.info("DB writer didn't stopped, shutting down");
                executorService.shutdownNow();
            }
        } catch (InterruptedException e) {
            LOGGER.warn("Exception occurred while trying to stop DBWriter: ", e);
            executorService.shutdownNow();
        }
        LOGGER.info("DB writer stopped");
        LOGGER.info("Total records processed: {}", numOfStorageRecords);
    }
}
