package org.practice.db;

import com.github.javafaker.Faker;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.practice.generators.ShopGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.print.Doc;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import static org.practice.App.DB_NAME;
import static org.practice.db.DBWriter.BATCH_SIZE;

public class ShopInitializer implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(ShopInitializer.class);
    public static final String SHOP_COLLECTION_NAME = "shops";
    private final MongoClient connection;
    private final Faker faker;
    private final int numOfShops;

    public ShopInitializer(MongoClient connection, Faker faker, int numOfShops) {
        this.connection = connection;
        this.faker = faker;
        this.numOfShops = numOfShops;
    }

    @Override
    public void run() {
        LOGGER.info("Shop Initializer started");
        MongoDatabase database = connection.getDatabase(DB_NAME);
        MongoCollection<Document> collection = database.getCollection(SHOP_COLLECTION_NAME);
        collection.drop();
        generateShops(collection, new ShopGenerator(faker), numOfShops);
        LOGGER.info("Shop Initializer stopped");
    }

    private void generateShops(MongoCollection<Document> collection, ShopGenerator generator, int numOfShops) {
        LOGGER.info("Start of shop generation");
        List<Document> list = new ArrayList<>();
        AtomicInteger counter = new AtomicInteger();
        Stream.generate(generator::generateShop)
                .limit(numOfShops)
                .forEach(shop -> {
                    Document address = new Document("city", shop.getCity());
                    address.append("street", shop.getStreet())
                            .append("house", shop.getHouseNum());
                    Document document = new Document("address", address);
                    list.add(document);
                    if (list.size() >= BATCH_SIZE) {
                        collection.insertMany(list);
                        counter.addAndGet(list.size());
                        LOGGER.info("Generated {} categories out of {}", counter.get(), numOfShops);
                        list.clear();
                    }
                });
        collection.insertMany(list);
        LOGGER.info("End of shop generation. Shops generated: {}", numOfShops);
    }
}
