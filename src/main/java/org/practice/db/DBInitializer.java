package org.practice.db;

import com.github.javafaker.Faker;
import com.mongodb.client.MongoClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class DBInitializer implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(DBInitializer.class);
    private final MongoClient connection;
    private final Faker faker;
    private final int numOfCategories;
    private final int numOfProducts;
    private final int numOfShops;

    public DBInitializer(MongoClient connection, Faker faker, int numOfCategories, int numOfProducts, int numOfShops) {
        this.connection = connection;
        this.faker = faker;
        this.numOfCategories = numOfCategories;
        this.numOfProducts = numOfProducts;
        this.numOfShops = numOfShops;
    }

    @Override
    public void run() {
        LOGGER.info("DB Initializer started");
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        executorService.execute(new ProductInitializer(connection, faker, numOfCategories, numOfProducts));
        executorService.execute(new ShopInitializer(connection, faker, numOfShops));
        executorService.shutdown();
        try {
            if (!executorService.awaitTermination(30, TimeUnit.SECONDS)) {
                LOGGER.info("DB initializer didn't stopped, shutting down");
                executorService.shutdownNow();
            }
        } catch (InterruptedException e) {
            LOGGER.warn("Exception occurred while trying to stop DBInitializer: ", e);
            executorService.shutdownNow();
        }
        LOGGER.info("DB initializer stopped");
    }
}
