package org.practice.generators;

import com.github.javafaker.Faker;
import org.bson.Document;
import org.practice.dto.Product;

import java.util.List;
import java.util.Random;

public class ProductGenerator {
    private static final Random random = new Random();
    private final List<Document> categoriesId;
    private final Faker faker;
    private final int numOfCategories;

    public ProductGenerator(Faker faker, List<Document> categoryIds, int numOfCategories) {
        this.faker = faker;
        this.categoriesId = categoryIds;
        this.numOfCategories = numOfCategories;
    }

    public Product generateProduct() {
        Product product = new Product();
        product.setName(faker.commerce().productName());
        product.setCategoryId(categoriesId.get(random.nextInt(numOfCategories)).getObjectId("_id"));
        return product;
    }
}
