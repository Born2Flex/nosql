package org.practice.generators;

import org.bson.Document;
import org.practice.dto.ProductShopRecord;

import java.util.List;
import java.util.Random;

public class ProductShopGenerator {
    private static final int MAX_PRODUCT_AMOUNT = 10_000;
    private final List<Document> categoryIds;
    private final List<Document> productIds;
    private final List<Document> shopIds;
    private static final Random random = new Random();
    private final int numOfCategories;
    private final int numOfProducts;
    private final int numOfShops;

    public ProductShopGenerator(List<Document> categoryIds, List<Document> productIds, List<Document> shopIds) {
        this.categoryIds = categoryIds;
        this.productIds = productIds;
        this.shopIds = shopIds;
        numOfCategories = categoryIds.size();
        numOfShops = shopIds.size();
        numOfProducts = productIds.size();
    }

    public ProductShopRecord generateProductShopRecord() {
        ProductShopRecord document = new ProductShopRecord();
        document.setCategoryId(categoryIds.get(random.nextInt(numOfCategories)).getObjectId("_id"));
        document.setProductId(productIds.get(random.nextInt(numOfProducts)).getObjectId("_id"));
        document.setShopId(shopIds.get(random.nextInt(numOfShops)).getObjectId("_id"));
        document.setAmount(random.nextInt(MAX_PRODUCT_AMOUNT) + 1);
        return document;
    }
}
