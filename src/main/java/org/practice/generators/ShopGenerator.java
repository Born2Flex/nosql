package org.practice.generators;

import com.github.javafaker.Faker;
import org.practice.dto.Shop;

public class ShopGenerator {
    private final Faker faker;

    public ShopGenerator(Faker faker) {
        this.faker = faker;
    }

    public Shop generateShop() {
        Shop shop = new Shop();
        shop.setCity(faker.address().cityName());
        shop.setStreet(faker.address().streetName());
        shop.setHouseNum(faker.address().buildingNumber());
        return shop;
    }
}
