package org.practice.properties;

import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Properties;


import static org.junit.jupiter.api.Assertions.assertEquals;

class PropertiesLoaderTest {

    @Test
    void loadPropertiesTest() {
        PropertiesLoader propertiesLoader = new PropertiesLoader();
        Properties properties = propertiesLoader.loadProperties();

        assertEquals(properties.getProperty("test"), "testValue");
        assertEquals(properties.getProperty("key"), "keyValue");
        assertEquals(properties.getProperty("value"), "project");
        assertEquals(properties.getProperty("url"), "http://ABCDE.com");
    }
}