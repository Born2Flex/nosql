package org.practice.db;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.practice.generators.ProductShopGenerator;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;
import static org.practice.db.DBWriter.STORAGE_COLLECTION_NAME;

@ExtendWith(MockitoExtension.class)
class DBWriterTest {
    @Mock
    private ProductShopGenerator generator;
    @Mock
    private MongoClient connection;

    @BeforeEach
    void setup() {
        Mockito.reset(generator, connection);
    }

    @Test
    void runTest() {
        List<Document> list = new ArrayList<>(List.of(new Document()));
        generator = new ProductShopGenerator(list, list, list);
        MongoDatabase db = mock(MongoDatabase.class);
        MongoCollection<Document> categoryCollection = mock(MongoCollection.class);
        MongoCollection<Document> productCollection = mock(MongoCollection.class);
        MongoCollection<Document> shopCollection = mock(MongoCollection.class);
        MongoCollection<Document> storageCollection = mock(MongoCollection.class);

        when(connection.getDatabase(anyString())).thenReturn(db);
        when(db.getCollection(anyString()))
                .thenReturn(categoryCollection)
                .thenReturn(productCollection)
                .thenReturn(shopCollection);
        when(db.getCollection(STORAGE_COLLECTION_NAME))
                .thenReturn(storageCollection);

        FindIterable<Document> document = mock(FindIterable.class);
        when(productCollection.find()).thenReturn(document);
        when(shopCollection.find()).thenReturn(document);
        when(document.projection(any())).thenReturn(document);
        Document doc = mock(Document.class);
        List<Document> documents = new ArrayList<>(List.of(doc));
        when(document.into(anyList())).thenReturn(documents);

        DBWriter writer = new DBWriter(connection, 45_000, 1, 1, 4);
        writer.run();
        verify(storageCollection, times(8)).insertMany(anyList());
    }
}