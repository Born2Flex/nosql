package org.practice.db;

import com.github.javafaker.Faker;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.practice.db.ShopInitializer;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ShopInitializerTest {
    private ShopInitializer shopInitializer;
    @Mock
    private MongoClient connection;
    @Mock
    private Faker faker;

    @BeforeEach
    void prepare() {
        Mockito.reset(connection, faker);
    }

    @Test
    void generateShopTest() {
        faker = new Faker();

        MongoDatabase db = mock(MongoDatabase.class);
        MongoCollection<Document> shopCollection = mock(MongoCollection.class);
        when(connection.getDatabase(anyString())).thenReturn(db);
        when(db.getCollection(anyString())).thenReturn(shopCollection);

        shopInitializer = new ShopInitializer(connection, faker, 10_000);
        shopInitializer.run();

        verify(shopCollection, times(2)).insertMany(anyList());
    }
}
