package org.practice.db;

import com.github.javafaker.Faker;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.practice.db.DBInitializer;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;
import static org.practice.db.ProductInitializer.CATEGORY_COLLECTION_NAME;
import static org.practice.db.ProductInitializer.PRODUCTS_COLLECTION_NAME;
import static org.practice.db.ShopInitializer.SHOP_COLLECTION_NAME;

@ExtendWith(MockitoExtension.class)
class DBInitializerTest {
    @Mock
    private MongoClient connection;
    @Mock
    private Faker faker;

    @BeforeEach
    void prepare() {
        Mockito.reset(connection, faker);
    }

    @Test
    void runTest() {
        faker = new Faker(new Locale("uk"));

        MongoDatabase db = mock(MongoDatabase.class);
        MongoCollection<Document> categoryCollection = mock(MongoCollection.class);
        MongoCollection<Document> productCollection = mock(MongoCollection.class);
        MongoCollection<Document> shopCollection = mock(MongoCollection.class);
        FindIterable<Document> mockDocument = mock(FindIterable.class);

        when(categoryCollection.find()).thenReturn(mockDocument);
        when(mockDocument.projection(any())).thenReturn(mockDocument);

        List<Document> resultList = new ArrayList<>();
        resultList.add(new Document());
        when(mockDocument.into(anyList())).thenReturn(resultList);

        when(connection.getDatabase(anyString())).thenReturn(db);
        when(db.getCollection(CATEGORY_COLLECTION_NAME)).thenReturn(categoryCollection);
        when(db.getCollection(PRODUCTS_COLLECTION_NAME)).thenReturn(productCollection);
        when(db.getCollection(SHOP_COLLECTION_NAME)).thenReturn(shopCollection);

        DBInitializer initializer = new DBInitializer(connection, faker, 1, 11_000, 23_000);
        initializer.run();

        verify(categoryCollection, times(1)).insertMany(any());
        verify(productCollection, times(2)).insertMany(any());
        verify(shopCollection, times(3)).insertMany(any());
    }
}
