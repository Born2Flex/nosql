package org.practice.db;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.practice.db.DBThread;
import org.practice.generators.ProductShopGenerator;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DBThreadTest {
    @Mock
    private ProductShopGenerator generator;
    @Mock
    private MongoClient connection;

    @BeforeEach
    void setup() {
        Mockito.reset(generator, connection);
    }

    @Test
    void runTest() {
        List<Document> list = new ArrayList<>(List.of(new Document()));
        generator = new ProductShopGenerator(list, list, list);
        MongoDatabase db = mock(MongoDatabase.class);
        MongoCollection<Document> storageCollection = mock(MongoCollection.class);

        when(connection.getDatabase(anyString())).thenReturn(db);
        when(db.getCollection(anyString())).thenReturn(storageCollection);

        DBThread thread = new DBThread(generator, connection, 20_000);
        thread.run();

        verify(storageCollection, times(3)).insertMany(anyList());
    }
}
