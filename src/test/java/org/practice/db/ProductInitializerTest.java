package org.practice.db;

import com.github.javafaker.Faker;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.practice.db.ProductInitializer;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ProductInitializerTest {
    private ProductInitializer productInitializer;
    @Mock
    private MongoClient connection;
    @Mock
    private Faker faker;

    @BeforeEach
    void prepare() {
        Mockito.reset(connection, faker);
    }

    @Test
    void generateCategoriesTest() {
        faker = new Faker(new Locale("uk"));

        MongoDatabase db = mock(MongoDatabase.class);
        MongoCollection<Document> categoryCollection = mock(MongoCollection.class);
        MongoCollection<Document> productCollection = mock(MongoCollection.class);
        FindIterable<Document> mockDocument = mock(FindIterable.class);

        when(categoryCollection.find()).thenReturn(mockDocument);
        when(mockDocument.projection(any())).thenReturn(mockDocument);

        List<Document> resultList = new ArrayList<>();
        resultList.add(new Document());
        when(mockDocument.into(anyList())).thenReturn(resultList);

        when(connection.getDatabase(anyString())).thenReturn(db);
        when(db.getCollection(anyString()))
                .thenReturn(categoryCollection)
                .thenReturn(productCollection);


        productInitializer = new ProductInitializer(connection, faker, 10_002, 0);
        productInitializer.run();

        verify(categoryCollection, times(2)).insertMany(any());
    }

    @Test
    void generateProductsTest() {
        faker = new Faker(new Locale("uk"));
        MongoDatabase db = mock(MongoDatabase.class);
        MongoCollection<Document> categoryCollection = mock(MongoCollection.class);
        MongoCollection<Document> productCollection = mock(MongoCollection.class);
        FindIterable<Document> mockDocument = mock(FindIterable.class);
        when(categoryCollection.find()).thenReturn(mockDocument);
        when(mockDocument.projection(any())).thenReturn(mockDocument);
        List<Document> resultList = new ArrayList<>();
        resultList.add(new Document());
        when(mockDocument.into(anyList())).thenReturn(resultList);

        when(connection.getDatabase(anyString())).thenReturn(db);
        when(db.getCollection(anyString()))
                .thenReturn(categoryCollection)
                .thenReturn(productCollection);


        productInitializer = new ProductInitializer(connection, faker, 1, 10_000);
        productInitializer.run();

        verify(productCollection, times(2)).insertMany(any());
    }
}
