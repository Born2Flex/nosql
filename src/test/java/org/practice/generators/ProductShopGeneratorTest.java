package org.practice.generators;

import org.bson.Document;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertTimeout;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ProductShopGeneratorTest {

    @Test
    void testTimeOfGeneration() {
        List<Document> products = mock(List.class);
        List<Document> shops = mock(List.class);
        List<Document> categories = mock(List.class);
        when(products.get(0)).thenReturn(new Document());
        when(shops.get(0)).thenReturn(new Document());
        when(categories.get(0)).thenReturn(new Document());
        ProductShopGenerator productGenerator = new ProductShopGenerator(categories, products, shops);
        assertTimeout(Duration.ofSeconds(1L), () -> {
            Stream.generate(productGenerator::generateProductShopRecord)
                    .limit(10000);
        });
    }

}