package org.practice.generators;

import com.github.javafaker.Faker;
import org.bson.Document;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertTimeout;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ProductGeneratorTest {
    @Test
    void testTimeOfGeneration() {
        Faker faker = new Faker();
        List<Document> list = mock(List.class);
        when(list.get(0)).thenReturn(new Document());
        int numOfCategories = 0;
        ProductGenerator productGenerator = new ProductGenerator(faker, list, numOfCategories);
        assertTimeout(Duration.ofSeconds(1L), () -> {
            Stream.generate(productGenerator::generateProduct)
                    .limit(10_000);
        });
    }
}